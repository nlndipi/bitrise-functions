import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
admin.initializeApp(functions.config().firebase);

export const sendNotif = functions.https.onRequest(
    async (request, response) => {
      if (request.body.build_status != 1) {
        response.status(200).json("No need for a notif on build start");
        return;
      }
      const buildSlug = request.body.build_slug;
      const appSlug = request.body.app_slug;
      const buildNumber = request.body.build_number;
      const dstBranch = request.body.git.dst_branch;
      const srcBranch: string = request.body.git.src_branch;

      if (srcBranch != dstBranch) {
        response.status(200).json("No need for a notif on pull request builds");
        return;
      }

      const apps = await admin.firestore().collection("apps").get();
      const app = apps.docs.find((doc) => doc.id == appSlug);

      const payload = {
        data: {
          body: `Build #${buildNumber} for ${app?.data().title} is ready`,
          title: "New Build",
          appSlug,
          buildSlug,
          dstBranch,
        },
      };

      const tokens: string[] = [];
      const result = await admin.firestore().collection("users").get();
      result.docs.forEach((doc) => {
        const data = doc.data();
        if (data.favorites.split(";").includes(appSlug)) {
          tokens.push(data.token);
        }
      });
      if (tokens.length > 0) {
        admin.messaging().sendToDevice(tokens, payload);
      }
      response.status(200).json("Sent the notification");
    },
);
